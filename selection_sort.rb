# O(n^2)

def selection_sort(arr)
  (0...arr.length).each do |i|
    min_idx = i
    (i...arr.length).each do |j|
      min_idx = j if arr[j] < arr[min_idx]
    end

    arr[i], arr[min_idx] = arr[min_idx], arr[i]
  end

  arr
end

arr = [9,9,8,3,2,1,1,0]

p selection_sort(arr)
