# O(n log n)
# VERSION 1

def merge_sort(arr, lo, hi)
  if hi <= lo
    return
  end

  mid = lo + (hi - lo) / 2
  merge_sort(arr, lo, mid)
  merge_sort(arr, mid + 1, hi)

  merge(arr, lo, mid, hi)
end

def merge(arr, lo, mid, hi)
  aux = arr.dup

  i, j = lo, mid + 1

  (lo..hi).each do |k|
    if i > mid
      arr[k] = aux[j]
      j += 1
    elsif j > hi
      arr[k] = aux[i]
      i += 1
    elsif aux[i] < aux[j]
      arr[k] = aux[i]
      i += 1
    else
      arr[k] = aux[j]
      j += 1
    end
  end
  arr
end

# VERSION 2

def merge_sort(arr)
  return arr if arr.length < 2
  
  mid = arr.length / 2
  left = merge_sort(arr.take(mid))
  right = merge_sort(arr.drop(mid))

  merge(left, right)
end

def merge(left, right)
  merged = []
  k, i, j = 0, 0, 0

  while i < left.length || j < right.length
    if i >= left.length
      merged[k] = right[j]
      j += 1
    elsif j >= right.length
      merged[k] = left[i]
      i += 1
    elsif left[i] < right[j]
      merged[k] = left[i]
      i += 1
    else
      merged[k] = right[j]
      j += 1
    end

    k += 1
  end

  merged
end

arr = [5,3,21,3,5,1,32,0]

# p merge_sort(arr)
# p merge_sort(arr, 0, arr.length - 1)
